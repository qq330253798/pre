import java.math.BigInteger;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Byte a1 = scan.nextByte();
        String str1 = scan.next();
        Byte a2 = scan.nextByte();
        String str2 = scan.next();
        Byte a3 = scan.nextByte();
        BigInteger bg1 = new BigInteger(str1,a1);
        BigInteger bg2 = new BigInteger(str2,a2);
        BigInteger bg3 = bg1.add(bg2);
        String s = bg3.toString();
        String s1 = new BigInteger(s,10).toString(a3);
        System.out.println(s1);
    }
}