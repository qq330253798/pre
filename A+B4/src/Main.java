import java.math.BigInteger;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str = scan.nextLine();
        String str1 = str.replaceAll(" ","");
        int l = 0;
        int flag;
        int k;
        String[] s = new String[10000];
        Pattern p = Pattern.compile("-+\\d+|" + "\\+\\-\\d+|"
                + "\\+\\d+|" + "\\-\\d+|" + "\\d+");
        Matcher m = p.matcher(str1);
        while (m.find()) {
            s[l++] = m.group();
        }
        BigInteger a = new BigInteger(s[0]);
        for (int i = 0;i < l;i++) {
            if (i != 0) {
                flag = 0;
                if (s[i].charAt(0) == '-' && s[i].charAt(1) == '+') {
                    k = 1;
                    flag = 1;
                }
                else if (s[i].charAt(0) == '-' && s[i].charAt(1) == '-') {
                    k = 1;
                    flag = 1;
                }
                else if (s[i].charAt(0) == '+' && s[i].charAt(1) == '-') {
                    k = 1;
                }
                else if (s[i].charAt(0) == '+' && s[i].charAt(1) == '+') {
                    k = 1;
                }
                else {
                    k = 0;
                }
                BigInteger b = new BigInteger(s[i].substring(k));

                if (flag == 0) {
                    a = a.add(b);
                }
                else {
                    a = a.subtract(b);
                }
            }
        }
        System.out.println(a);
    }
}
