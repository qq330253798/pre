import java.math.BigInteger;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String str1 = scan.next();
        String str2 = scan.next();
        String str3 = scan.nextLine();
        int flag = 0;
        for (int i = 0;i < str1.length();i++) {
            if (i == 0) {
                if ((str1.charAt(0) == '+' || str1.charAt(0) == '-')
                        || (str1.charAt(0) >= '0' && str1.charAt(0) <= '9')) {
                    continue;
                }
                flag = 1;
            }
            else if ((str1.charAt(i) >= '0') && (str1.charAt(i) <= '9')) {
                continue;
            }
            else {
                flag = 1;
            }
        }
        for (int i = 0;i < str2.length();i++) {
            if (i == 0) {
                if ((str2.charAt(0) == '+' || str2.charAt(0) == '-')
                        || (str2.charAt(0) >= '0' && str2.charAt(0) <= '9')) {
                    continue;
                }
                flag = 1;
            }
            else if ((str2.charAt(i) >= '0') && (str2.charAt(i) <= '9')) {
                continue;
            }
            else {
                flag = 1;
            }
        }
        if (!str3.equals("") || flag == 1) {
            System.out.println("WRONG FORMAT!");
        }
        else {
            BigInteger bg1 = new BigInteger(str1);
            BigInteger bg2 = new BigInteger(str2);
            System.out.println(bg1.add(bg2));
        }

    }
}
